# !/user/bin/env python
# coding=utf-8
import redis,openpyxl,json,phpserialize
from app_thread import *

redis_obj = get_redis(0)

all_key = redis_obj.keys('*')

excel = []
i=0
for x in all_key:
    data_json = redis_obj.get(x)
    data = json.loads(data_json)
    excel.append([
        data['id'],
        data['cover'],
        data['qiniu_cover'],
        data['small_videl'],
        data['qiniu_small_videl'],
        data['url'],
        data['detail'],
        data['description'],
        data['tag_list'],
        data['video_url'],
        data['share_url'],
        phpserialize.dumps(data['attr1'], charset="utf-8"),
        phpserialize.dumps(data['attr2'], charset="utf-8")
    ])
    i+=1

wb = openpyxl.Workbook()
ws = wb.active
ws['A1'] = 'ID'
ws['B1'] = '封面图片'
ws['C1'] = '封面七牛'
ws['D1'] = '封面短视频'
ws['E1'] = '七牛封面短视频'
ws['F1'] = '原始链接'
ws['G1'] = '简介'
ws['H1'] = '描述'
ws['I1'] = '标签'
ws['J1'] = '视频地址'
ws['K1'] = '文件'
ws['L1'] = '属性1'
ws['M1'] = '属性2'
for gov in excel:
#     # 写入多个单元格，已经存在的文件覆盖,第几次写入，就写在第几行
    ws.append(gov)
wb.save('mixkit_data.xlsx')