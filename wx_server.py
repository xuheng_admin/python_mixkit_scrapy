#!/usr/bin/python
# -*- coding: UTF-8 -*-

import json
import requests
from flask import Flask, request

web = requests.session()
web.headers['Content-Type'] = 'application/x-www-form-urlencoded'

app = Flask(__name__)


@app.route('/msg', methods=['POST'])
def jieshou():
    data = request.get_data()  # 字节流
    jsondata = json.loads(data)
    content = jsondata['content']
    msgtype = jsondata['msgtype']
    sender = jsondata["sender"]  # 群消息的发送人id
    wxid = jsondata["wxid"]  # 可能是群或者个人的wxid
    print(content, content, msgtype, sender, wxid)

    return ''


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8888, debug=True, threaded=True)
